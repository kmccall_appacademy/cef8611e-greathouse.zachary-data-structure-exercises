# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = ['a','e','i','o','u']
  count = 0

  str.split("").each do |char|
    if vowels.include? char.downcase
      count += 1
    end
  end
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = ['a','e','i','o','u']

  str.split("").each do |char|
    if vowels.include? char.downcase
      str.delete!(char)
    end
  end
  str
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  string_conversion = int.to_s
  split_string = string_conversion.split("")
  split_string.sort.reverse

end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  str.downcase!

  str.split("").each do |letter|
    if str.count(letter) > 1
      return true
    end
  end
  return false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  area_code = arr[0..2].join("")
  part_one = arr[3..5].join("")
  part_two = arr[6..9].join("")

  phone_number = "(#{area_code}) #{part_one}-#{part_two}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  split_string = str.split(",")
  int_converted_elements = []

  split_string.each do |num|
    int_converted_elements.push(num.to_i)
  end

  range(int_converted_elements)
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  if offset >= 0 && offset < arr.length
    arr.drop(offset) + arr.take(offset)

  elsif offset >= 0 && offset > arr.length
    offset = offset % arr.length
    arr.drop(offset) + arr.take(offset)

  elsif offset < 0
    arr.drop(arr.length + offset) + arr.take(arr.length + offset)
  end
end
